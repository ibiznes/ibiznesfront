//gridslider_start
/*
Ether Gridslider 1.4.0 jQuery Plugin for jQuery 1.5+
created: May 2012
latest update: August 6, 2013
copyright: Por Design 2012
contact: contact.pordesign@gmail.com
website: http://ether-wp.com/ or http://pordesign.eu/
buy license at: http://codecanyon.net/item/ether-grid-slider-jquery-plugin/1713182
*/
//////

(function($)
{
	"use strict";

	var ua = navigator.userAgent.toLowerCase();
	var isie = /msie/.test(ua);
	var iev = parseFloat((ua.match(/.*(?:rv|ie)[\/: ](.+?)([ \);]|$)/) || [])[1]);

	if(!jQuery.easing.easeInQuad)
	{
jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(e,a,c,b,d){return jQuery.easing[jQuery.easing.def](e,a,c,b,d)},easeInQuad:function(e,a,c,b,d){return b*(a/=d)*a+c},easeOutQuad:function(e,a,c,b,d){return-b*(a/=d)*(a-2)+c},easeInOutQuad:function(e,a,c,b,d){if((a/=d/2)<1)return b/2*a*a+c;return-b/2*(--a*(a-2)-1)+c},easeInCubic:function(e,a,c,b,d){return b*(a/=d)*a*a+c},easeOutCubic:function(e,a,c,b,d){return b*((a=a/d-1)*a*a+1)+c},easeInOutCubic:function(e,a,c,b,d){if((a/=d/2)<1)return b/2*a*a*a+c;return b/2*((a-=2)*a*a+2)+c},easeInQuart:function(e,a,c,b,d){return b*(a/=d)*a*a*a+c},easeOutQuart:function(e,a,c,b,d){return-b*((a=a/d-1)*a*a*a-1)+c},easeInOutQuart:function(e,a,c,b,d){if((a/=d/2)<1)return b/2*a*a*a*a+c;return-b/2*((a-=2)*a*a*a-2)+c},easeInQuint:function(e,a,c,b,d){return b*(a/=d)*a*a*a*a+c},easeOutQuint:function(e,a,c,b,d){return b*((a=a/d-1)*a*a*a*a+1)+c},easeInOutQuint:function(e,a,c,b,d){if((a/=d/2)<1)return b/2*a*a*a*a*a+c;return b/2*((a-=2)*a*a*a*a+2)+c},easeInSine:function(e,
a,c,b,d){return-b*Math.cos(a/d*(Math.PI/2))+b+c},easeOutSine:function(e,a,c,b,d){return b*Math.sin(a/d*(Math.PI/2))+c},easeInOutSine:function(e,a,c,b,d){return-b/2*(Math.cos(Math.PI*a/d)-1)+c},easeInExpo:function(e,a,c,b,d){return a==0?c:b*Math.pow(2,10*(a/d-1))+c},easeOutExpo:function(e,a,c,b,d){return a==d?c+b:b*(-Math.pow(2,-10*a/d)+1)+c},easeInOutExpo:function(e,a,c,b,d){if(a==0)return c;if(a==d)return c+b;if((a/=d/2)<1)return b/2*Math.pow(2,10*(a-1))+c;return b/2*(-Math.pow(2,-10*--a)+2)+c},
easeInCirc:function(e,a,c,b,d){return-b*(Math.sqrt(1-(a/=d)*a)-1)+c},easeOutCirc:function(e,a,c,b,d){return b*Math.sqrt(1-(a=a/d-1)*a)+c},easeInOutCirc:function(e,a,c,b,d){if((a/=d/2)<1)return-b/2*(Math.sqrt(1-a*a)-1)+c;return b/2*(Math.sqrt(1-(a-=2)*a)+1)+c},easeInElastic:function(e,a,c,b,d){e=1.70158;var f=0,g=b;if(a==0)return c;if((a/=d)==1)return c+b;f||(f=d*0.3);if(g<Math.abs(b)){g=b;e=f/4}else e=f/(2*Math.PI)*Math.asin(b/g);return-(g*Math.pow(2,10*(a-=1))*Math.sin((a*d-e)*2*Math.PI/f))+c},easeOutElastic:function(e,
a,c,b,d){e=1.70158;var f=0,g=b;if(a==0)return c;if((a/=d)==1)return c+b;f||(f=d*0.3);if(g<Math.abs(b)){g=b;e=f/4}else e=f/(2*Math.PI)*Math.asin(b/g);return g*Math.pow(2,-10*a)*Math.sin((a*d-e)*2*Math.PI/f)+b+c},easeInOutElastic:function(e,a,c,b,d){e=1.70158;var f=0,g=b;if(a==0)return c;if((a/=d/2)==2)return c+b;f||(f=d*0.3*1.5);if(g<Math.abs(b)){g=b;e=f/4}else e=f/(2*Math.PI)*Math.asin(b/g);if(a<1)return-0.5*g*Math.pow(2,10*(a-=1))*Math.sin((a*d-e)*2*Math.PI/f)+c;return g*Math.pow(2,-10*(a-=1))*Math.sin((a*
d-e)*2*Math.PI/f)*0.5+b+c},easeInBack:function(e,a,c,b,d,f){if(f==undefined)f=1.70158;return b*(a/=d)*a*((f+1)*a-f)+c},easeOutBack:function(e,a,c,b,d,f){if(f==undefined)f=1.70158;return b*((a=a/d-1)*a*((f+1)*a+f)+1)+c},easeInOutBack:function(e,a,c,b,d,f){if(f==undefined)f=1.70158;if((a/=d/2)<1)return b/2*a*a*(((f*=1.525)+1)*a-f)+c;return b/2*((a-=2)*a*(((f*=1.525)+1)*a+f)+2)+c},easeInBounce:function(e,a,c,b,d){return b-jQuery.easing.easeOutBounce(e,d-a,0,b,d)+c},easeOutBounce:function(e,a,c,b,d){return(a/=
d)<1/2.75?b*7.5625*a*a+c:a<2/2.75?b*(7.5625*(a-=1.5/2.75)*a+0.75)+c:a<2.5/2.75?b*(7.5625*(a-=2.25/2.75)*a+0.9375)+c:b*(7.5625*(a-=2.625/2.75)*a+0.984375)+c},easeInOutBounce:function(e,a,c,b,d){if(a<d/2)return jQuery.easing.easeInBounce(e,a*2,0,b,d)*0.5+c;return jQuery.easing.easeOutBounce(e,a*2-d,0,b,d)*0.5+b*0.5+c}});
	}

	if(!jQuery.mousewheel)
	{
		(function(d){var b=["DOMMouseScroll","mousewheel"];if(d.event.fixHooks){for(var a=b.length;a;){d.event.fixHooks[b[--a]]=d.event.mouseHooks}}d.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var e=b.length;e;){this.addEventListener(b[--e],c,false)}}else{this.onmousewheel=c}},teardown:function(){if(this.removeEventListener){for(var e=b.length;e;){this.removeEventListener(b[--e],c,false)}}else{this.onmousewheel=null}}};d.fn.extend({mousewheel:function(e){return e?this.bind("mousewheel",e):this.trigger("mousewheel")},unmousewheel:function(e){return this.unbind("mousewheel",e)}});function c(j){var h=j||window.event,g=[].slice.call(arguments,1),k=0,i=true,f=0,e=0;j=d.event.fix(h);j.type="mousewheel";if(h.wheelDelta){k=h.wheelDelta/120}if(h.detail){k=-h.detail/3}e=k;if(h.axis!==undefined&&h.axis===h.HORIZONTAL_AXIS){e=0;f=-1*k}if(h.wheelDeltaY!==undefined){e=h.wheelDeltaY/120}if(h.wheelDeltaX!==undefined){f=-1*h.wheelDeltaX/120}g.unshift(j,k,f,e);return(d.event.dispatch||d.event.handle).apply(this,g)}})(jQuery);
	}

	if ( ! isie || iev > 8)
	{
		if (!jQuery.swipe)
		{
			(function(e){var o="left",n="right",d="up",v="down",c="in",w="out",l="none",r="auto",k="swipe",s="pinch",x="tap",i="doubletap",b="longtap",A="horizontal",t="vertical",h="all",q=10,f="start",j="move",g="end",p="cancel",a="ontouchstart" in window,y="TouchSwipe";var m={fingers:1,threshold:75,cancelThreshold:null,pinchThreshold:20,maxTimeThreshold:null,fingerReleaseThreshold:250,longTapThreshold:500,doubleTapThreshold:200,swipe:null,swipeLeft:null,swipeRight:null,swipeUp:null,swipeDown:null,swipeStatus:null,pinchIn:null,pinchOut:null,pinchStatus:null,click:null,tap:null,doubleTap:null,longTap:null,triggerOnTouchEnd:true,triggerOnTouchLeave:false,allowPageScroll:"auto",fallbackToMouseEvents:true,excludedElements:"button, input, select, textarea, a, .noSwipe"};e.fn.swipe=function(D){var C=e(this),B=C.data(y);if(B&&typeof D==="string"){if(B[D]){return B[D].apply(this,Array.prototype.slice.call(arguments,1))}else{e.error("Method "+D+" does not exist on jQuery.swipe")}}else{if(!B&&(typeof D==="object"||!D)){return u.apply(this,arguments)}}return C};e.fn.swipe.defaults=m;e.fn.swipe.phases={PHASE_START:f,PHASE_MOVE:j,PHASE_END:g,PHASE_CANCEL:p};e.fn.swipe.directions={LEFT:o,RIGHT:n,UP:d,DOWN:v,IN:c,OUT:w};e.fn.swipe.pageScroll={NONE:l,HORIZONTAL:A,VERTICAL:t,AUTO:r};e.fn.swipe.fingers={ONE:1,TWO:2,THREE:3,ALL:h};function u(B){if(B&&(B.allowPageScroll===undefined&&(B.swipe!==undefined||B.swipeStatus!==undefined))){B.allowPageScroll=l}if(B.click!==undefined&&B.tap===undefined){B.tap=B.click}if(!B){B={}}B=e.extend({},e.fn.swipe.defaults,B);return this.each(function(){var D=e(this);var C=D.data(y);if(!C){C=new z(this,B);D.data(y,C)}})}function z(a0,aq){var av=(a||!aq.fallbackToMouseEvents),G=av?"touchstart":"mousedown",au=av?"touchmove":"mousemove",R=av?"touchend":"mouseup",P=av?null:"mouseleave",az="touchcancel";var ac=0,aL=null,Y=0,aX=0,aV=0,D=1,am=0,aF=0,J=null;var aN=e(a0);var W="start";var T=0;var aM=null;var Q=0,aY=0,a1=0,aa=0,K=0;var aS=null;try{aN.bind(G,aJ);aN.bind(az,a5)}catch(ag){e.error("events not supported "+G+","+az+" on jQuery.swipe")}this.enable=function(){aN.bind(G,aJ);aN.bind(az,a5);return aN};this.disable=function(){aG();return aN};this.destroy=function(){aG();aN.data(y,null);return aN};this.option=function(a8,a7){if(aq[a8]!==undefined){if(a7===undefined){return aq[a8]}else{aq[a8]=a7}}else{e.error("Option "+a8+" does not exist on jQuery.swipe.options")}};function aJ(a9){if(ax()){return}if(e(a9.target).closest(aq.excludedElements,aN).length>0){return}var ba=a9.originalEvent?a9.originalEvent:a9;var a8,a7=a?ba.touches[0]:ba;W=f;if(a){T=ba.touches.length}else{a9.preventDefault()}ac=0;aL=null;aF=null;Y=0;aX=0;aV=0;D=1;am=0;aM=af();J=X();O();if(!a||(T===aq.fingers||aq.fingers===h)||aT()){ae(0,a7);Q=ao();if(T==2){ae(1,ba.touches[1]);aX=aV=ap(aM[0].start,aM[1].start)}if(aq.swipeStatus||aq.pinchStatus){a8=L(ba,W)}}else{a8=false}if(a8===false){W=p;L(ba,W);return a8}else{ak(true)}}function aZ(ba){var bd=ba.originalEvent?ba.originalEvent:ba;if(W===g||W===p||ai()){return}var a9,a8=a?bd.touches[0]:bd;var bb=aD(a8);aY=ao();if(a){T=bd.touches.length}W=j;if(T==2){if(aX==0){ae(1,bd.touches[1]);aX=aV=ap(aM[0].start,aM[1].start)}else{aD(bd.touches[1]);aV=ap(aM[0].end,aM[1].end);aF=an(aM[0].end,aM[1].end)}D=a3(aX,aV);am=Math.abs(aX-aV)}if((T===aq.fingers||aq.fingers===h)||!a||aT()){aL=aH(bb.start,bb.end);ah(ba,aL);ac=aO(bb.start,bb.end);Y=aI();aE(aL,ac);if(aq.swipeStatus||aq.pinchStatus){a9=L(bd,W)}if(!aq.triggerOnTouchEnd||aq.triggerOnTouchLeave){var a7=true;if(aq.triggerOnTouchLeave){var bc=aU(this);a7=B(bb.end,bc)}if(!aq.triggerOnTouchEnd&&a7){W=ay(j)}else{if(aq.triggerOnTouchLeave&&!a7){W=ay(g)}}if(W==p||W==g){L(bd,W)}}}else{W=p;L(bd,W)}if(a9===false){W=p;L(bd,W)}}function I(a7){var a8=a7.originalEvent;if(a){if(a8.touches.length>0){C();return true}}if(ai()){T=aa}a7.preventDefault();aY=ao();Y=aI();if(a6()){W=p;L(a8,W)}else{if(aq.triggerOnTouchEnd||(aq.triggerOnTouchEnd==false&&W===j)){W=g;L(a8,W)}else{if(!aq.triggerOnTouchEnd&&a2()){W=g;aB(a8,W,x)}else{if(W===j){W=p;L(a8,W)}}}}ak(false)}function a5(){T=0;aY=0;Q=0;aX=0;aV=0;D=1;O();ak(false)}function H(a7){var a8=a7.originalEvent;if(aq.triggerOnTouchLeave){W=ay(g);L(a8,W)}}function aG(){aN.unbind(G,aJ);aN.unbind(az,a5);aN.unbind(au,aZ);aN.unbind(R,I);if(P){aN.unbind(P,H)}ak(false)}function ay(bb){var ba=bb;var a9=aw();var a8=aj();var a7=a6();if(!a9||a7){ba=p}else{if(a8&&bb==j&&(!aq.triggerOnTouchEnd||aq.triggerOnTouchLeave)){ba=g}else{if(!a8&&bb==g&&aq.triggerOnTouchLeave){ba=p}}}return ba}function L(a9,a7){var a8=undefined;if(F()||S()){a8=aB(a9,a7,k)}else{if((M()||aT())&&a8!==false){a8=aB(a9,a7,s)}}if(aC()&&a8!==false){a8=aB(a9,a7,i)}else{if(al()&&a8!==false){a8=aB(a9,a7,b)}else{if(ad()&&a8!==false){a8=aB(a9,a7,x)}}}if(a7===p){a5(a9)}if(a7===g){if(a){if(a9.touches.length==0){a5(a9)}}else{a5(a9)}}return a8}function aB(ba,a7,a9){var a8=undefined;if(a9==k){aN.trigger("swipeStatus",[a7,aL||null,ac||0,Y||0,T]);if(aq.swipeStatus){a8=aq.swipeStatus.call(aN,ba,a7,aL||null,ac||0,Y||0,T);if(a8===false){return false}}if(a7==g&&aR()){aN.trigger("swipe",[aL,ac,Y,T]);if(aq.swipe){a8=aq.swipe.call(aN,ba,aL,ac,Y,T);if(a8===false){return false}}switch(aL){case o:aN.trigger("swipeLeft",[aL,ac,Y,T]);if(aq.swipeLeft){a8=aq.swipeLeft.call(aN,ba,aL,ac,Y,T)}break;case n:aN.trigger("swipeRight",[aL,ac,Y,T]);if(aq.swipeRight){a8=aq.swipeRight.call(aN,ba,aL,ac,Y,T)}break;case d:aN.trigger("swipeUp",[aL,ac,Y,T]);if(aq.swipeUp){a8=aq.swipeUp.call(aN,ba,aL,ac,Y,T)}break;case v:aN.trigger("swipeDown",[aL,ac,Y,T]);if(aq.swipeDown){a8=aq.swipeDown.call(aN,ba,aL,ac,Y,T)}break}}}if(a9==s){aN.trigger("pinchStatus",[a7,aF||null,am||0,Y||0,T,D]);if(aq.pinchStatus){a8=aq.pinchStatus.call(aN,ba,a7,aF||null,am||0,Y||0,T,D);if(a8===false){return false}}if(a7==g&&a4()){switch(aF){case c:aN.trigger("pinchIn",[aF||null,am||0,Y||0,T,D]);if(aq.pinchIn){a8=aq.pinchIn.call(aN,ba,aF||null,am||0,Y||0,T,D)}break;case w:aN.trigger("pinchOut",[aF||null,am||0,Y||0,T,D]);if(aq.pinchOut){a8=aq.pinchOut.call(aN,ba,aF||null,am||0,Y||0,T,D)}break}}}if(a9==x){if(a7===p||a7===g){clearTimeout(aS);if(V()&&!E()){K=ao();aS=setTimeout(e.proxy(function(){K=null;aN.trigger("tap",[ba.target]);if(aq.tap){a8=aq.tap.call(aN,ba,ba.target)}},this),aq.doubleTapThreshold)}else{K=null;aN.trigger("tap",[ba.target]);if(aq.tap){a8=aq.tap.call(aN,ba,ba.target)}}}}else{if(a9==i){if(a7===p||a7===g){clearTimeout(aS);K=null;aN.trigger("doubletap",[ba.target]);if(aq.doubleTap){a8=aq.doubleTap.call(aN,ba,ba.target)}}}else{if(a9==b){if(a7===p||a7===g){clearTimeout(aS);K=null;aN.trigger("longtap",[ba.target]);if(aq.longTap){a8=aq.longTap.call(aN,ba,ba.target)}}}}}return a8}function aj(){var a7=true;if(aq.threshold!==null){a7=ac>=aq.threshold}return a7}function a6(){var a7=false;if(aq.cancelThreshold!==null&&aL!==null){a7=(aP(aL)-ac)>=aq.cancelThreshold}return a7}function ab(){if(aq.pinchThreshold!==null){return am>=aq.pinchThreshold}return true}function aw(){var a7;if(aq.maxTimeThreshold){if(Y>=aq.maxTimeThreshold){a7=false}else{a7=true}}else{a7=true}return a7}function ah(a7,a8){if(aq.allowPageScroll===l||aT()){a7.preventDefault()}else{var a9=aq.allowPageScroll===r;switch(a8){case o:if((aq.swipeLeft&&a9)||(!a9&&aq.allowPageScroll!=A)){a7.preventDefault()}break;case n:if((aq.swipeRight&&a9)||(!a9&&aq.allowPageScroll!=A)){a7.preventDefault()}break;case d:if((aq.swipeUp&&a9)||(!a9&&aq.allowPageScroll!=t)){a7.preventDefault()}break;case v:if((aq.swipeDown&&a9)||(!a9&&aq.allowPageScroll!=t)){a7.preventDefault()}break}}}function a4(){var a8=aK();var a7=U();var a9=ab();return a8&&a7&&a9}function aT(){return !!(aq.pinchStatus||aq.pinchIn||aq.pinchOut)}function M(){return !!(a4()&&aT())}function aR(){var ba=aw();var bc=aj();var a9=aK();var a7=U();var a8=a6();var bb=!a8&&a7&&a9&&bc&&ba;return bb}function S(){return !!(aq.swipe||aq.swipeStatus||aq.swipeLeft||aq.swipeRight||aq.swipeUp||aq.swipeDown)}function F(){return !!(aR()&&S())}function aK(){return((T===aq.fingers||aq.fingers===h)||!a)}function U(){return aM[0].end.x!==0}function a2(){return !!(aq.tap)}function V(){return !!(aq.doubleTap)}function aQ(){return !!(aq.longTap)}function N(){if(K==null){return false}var a7=ao();return(V()&&((a7-K)<=aq.doubleTapThreshold))}function E(){return N()}function at(){return((T===1||!a)&&(isNaN(ac)||ac===0))}function aW(){return((Y>aq.longTapThreshold)&&(ac<q))}function ad(){return !!(at()&&a2())}function aC(){return !!(N()&&V())}function al(){return !!(aW()&&aQ())}function C(){a1=ao();aa=event.touches.length+1}function O(){a1=0;aa=0}function ai(){var a7=false;if(a1){var a8=ao()-a1;if(a8<=aq.fingerReleaseThreshold){a7=true}}return a7}function ax(){return !!(aN.data(y+"_intouch")===true)}function ak(a7){if(a7===true){aN.bind(au,aZ);aN.bind(R,I);if(P){aN.bind(P,H)}}else{aN.unbind(au,aZ,false);aN.unbind(R,I,false);if(P){aN.unbind(P,H,false)}}aN.data(y+"_intouch",a7===true)}function ae(a8,a7){var a9=a7.identifier!==undefined?a7.identifier:0;aM[a8].identifier=a9;aM[a8].start.x=aM[a8].end.x=a7.pageX||a7.clientX;aM[a8].start.y=aM[a8].end.y=a7.pageY||a7.clientY;return aM[a8]}function aD(a7){var a9=a7.identifier!==undefined?a7.identifier:0;var a8=Z(a9);a8.end.x=a7.pageX||a7.clientX;a8.end.y=a7.pageY||a7.clientY;return a8}function Z(a8){for(var a7=0;a7<aM.length;a7++){if(aM[a7].identifier==a8){return aM[a7]}}}function af(){var a7=[];for(var a8=0;a8<=5;a8++){a7.push({start:{x:0,y:0},end:{x:0,y:0},identifier:0})}return a7}function aE(a7,a8){a8=Math.max(a8,aP(a7));J[a7].distance=a8}function aP(a7){return J[a7].distance}function X(){var a7={};a7[o]=ar(o);a7[n]=ar(n);a7[d]=ar(d);a7[v]=ar(v);return a7}function ar(a7){return{direction:a7,distance:0}}function aI(){return aY-Q}function ap(ba,a9){var a8=Math.abs(ba.x-a9.x);var a7=Math.abs(ba.y-a9.y);return Math.round(Math.sqrt(a8*a8+a7*a7))}function a3(a7,a8){var a9=(a8/a7)*1;return a9.toFixed(2)}function an(){if(D<1){return w}else{return c}}function aO(a8,a7){return Math.round(Math.sqrt(Math.pow(a7.x-a8.x,2)+Math.pow(a7.y-a8.y,2)))}function aA(ba,a8){var a7=ba.x-a8.x;var bc=a8.y-ba.y;var a9=Math.atan2(bc,a7);var bb=Math.round(a9*180/Math.PI);if(bb<0){bb=360-Math.abs(bb)}return bb}function aH(a8,a7){var a9=aA(a8,a7);if((a9<=45)&&(a9>=0)){return o}else{if((a9<=360)&&(a9>=315)){return o}else{if((a9>=135)&&(a9<=225)){return n}else{if((a9>45)&&(a9<135)){return v}else{return d}}}}}function ao(){var a7=new Date();return a7.getTime()}function aU(a7){a7=e(a7);var a9=a7.offset();var a8={left:a9.left,right:a9.left+a7.outerWidth(),top:a9.top,bottom:a9.top+a7.outerHeight()};return a8}function B(a7,a8){return(a7.x>a8.left&&a7.x<a8.right&&a7.y>a8.top&&a7.y<a8.bottom)}}})(jQuery);
		}
	}

	window.egs = {};
	egs.elem_cfg = {};
	egs.prefix = 'ether';
	egs.window_ref = $(window);
	egs.placeholder_img_data = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAMAAAAoyzS7AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjQwNkI5RDRFNjFBQzExRTE5MjJDRjRGMUM2MTdDODUyIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjQwNkI5RDRGNjFBQzExRTE5MjJDRjRGMUM2MTdDODUyIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NDA2QjlENEM2MUFDMTFFMTkyMkNGNEYxQzYxN0M4NTIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NDA2QjlENEQ2MUFDMTFFMTkyMkNGNEYxQzYxN0M4NTIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4G1oNaAAAABlBMVEX///8AAABVwtN+AAAADElEQVR42mJgAAgwAAACAAFPbVnhAAAAAElFTkSuQmCC';

	egs.add_prefix = function (string)
	{
		if (egs.prefix && egs.prefix !== '')
		{
			return egs.prefix + '-' + string;
		} else
		{
			return string;
		}
	}


	egs.log = function (msg)
	{
		! egs.msg ? egs.msg = [msg] : egs.msg.push(msg);
		! $('body').find('#egs.log').length ? $('body').append('<div id="//egs.log" style="font-size: 8pt; position: absolute; top: 0; left: 0; width: 200px; background: rgb(0,0,0); z-index: 999999999"></div>') : '';
		$('body').find('#egs.log').append('<p style="margin: 0; padding: 2px 10px; font-size: 9pt; color: #fff;">' + egs.msg.length + ': ' + msg + '</p>');
		$('body').find('#egs.log').children().length > 10 ? $('body').find('#egs.log').children().eq(0).remove() : '';
	}

	egs.in_view = function($elem)
	{
		var e_t = $elem.offset().top;
		var e_h = $elem.outerHeight();
		var w_t = $(window).scrollTop();
		var w_h = $(window).height();

		if ((e_t + e_h < w_t + w_h && e_t + e_h > w_t) || (e_t < w_t + w_h && e_t > w_t))
		{
			return true;
		} else
		{
			return false;
		}
	}

	egs.set_slider_window_height = function ($elem, cfg)
	{
		if (cfg.slider)
		{
			cfg.elem_height = cfg.col_group_elems_height[cfg.view_pos];
			//egs.log('set_slider_window_hegiht')
			//egs.log(cfg.$slider_window)
			cfg.$slider_window
				.stop(true, true)
				.animate({height: cfg.col_group_elems_height[cfg.view_pos]}, cfg.scroll_speed)
				.queue(function ()
				{
					if (! cfg.shift_busy || cfg.shift_busy === 0)
					{
						$(this).css({overflow: 'visible'});
					}

					$(this).dequeue();
				});		
		}
	}

	egs.apply_shift = function ($elem, cfg)
	{
		//egs.log('apply shift start')
		var scroll_axis = ['x', 'y', 'z'];
		var transition = ['slide', 'slideIn', 'slideOut', 'switch', 'swap'];
		var random_axis;
		var random_transition;
		var x1 = 0;
		var y1 = 0;
		var x2 = 0;
		var y2 = 0;
		var invert1 = 1;
		var invert2 = 1;

		if (cfg.transition === 'random')
		{
			random_transition = true;
			cfg.transition = transition[Math.ceil(Math.random() * transition.length - 1)];
		}

		if (cfg.scroll_axis === 'random')
		{
			random_axis = true;
			cfg.scroll_axis = scroll_axis[Math.ceil(Math.random() * scroll_axis.length - 1)];
		}

		if (cfg.scroll_axis === 'x')
		{
			x1 = 1;
			x2 = 1;
		} else if (cfg.scroll_axis === 'y')
		{
			y1 = 1;
			y2 = 1;
		}

		if (cfg.transition === 'slideIn')
		{
			x2 = 0;
			y2 = 0;
		} else if (cfg.transition === 'slideOut')
		{
			x1 = 0;
			y1 = 0;
		} else if (cfg.transition === 'switch' || cfg.transition === 'swap' || cfg.transition === 'shuffle')
		{
			invert2 = -1;
		}

		if (random_transition === true)
		{
			cfg.transition = 'random';
		}

		if (random_axis === true)
		{
			cfg.scroll_axis = 'random';
		}
		//egs.log('apply shift ')
		
		cfg.$col_group_elems
			.eq(cfg.view_pos)
				.css({ left: cfg.shift_dir * cfg.elem_width * x1 * invert1, top: cfg.shift_dir * cfg.col_group_elems_height[cfg.view_pos] * y1 * invert1, visibility: 'visible', 'z-index': 10, opacity: 0 })
				.animate({ left: 0, top: 0, opacity: 1}, cfg.scroll_speed, cfg.easing)
				.end()
			.eq(cfg.prev_view_pos)
				.css({ left: 0, top: 0 })
				.animate({ left: -cfg.shift_dir * cfg.elem_width * x2 * invert2, top: -cfg.shift_dir * cfg.col_group_elems_height[cfg.view_pos] * y2 * invert2, opacity: 0 }, cfg.scroll_speed, cfg.easing, function ()
				{
					$(this).css({ visibility: 'hidden', 'z-index': -1 })
				})

		egs.set_slider_window_height($elem, cfg);
		egs.update_slider_ctrl($elem, cfg);

		var t = setTimeout(function ()
		{
			cfg.$slider_window.css({overflow: 'visible'});
			cfg.shift_busy = 0;
		}, cfg.scroll_speed);
		//egs.log('apply_shift_end')
	}

	egs.init_shift = function ($elem, cfg, shift_type, shift_dest)
	{
		if (shift_type === 'absolute' && shift_dest === cfg.view_pos)
		{
			return false;
		}

		if (cfg.col_group_elems_count === 1)
		{
			return false;
		}

		if (cfg.shift_busy !== 1)
		{
			cfg.shift_dir = function ()
			{
				if (shift_type === 'absolute')
				{
					if (shift_dest > cfg.view_pos)
					{
						return 1;
					} else if (shift_dest < cfg.view_pos)
					{
						return -1;
					} else
					{
						return 0;
					}
				} else if (shift_type === 'relative')
				{
					return shift_dest;
				}
			}();

			cfg.prev_view_pos = cfg.view_pos;

			cfg.view_pos = function ()
			{
				if (shift_type === 'relative')
				{
					if (cfg.loop === true)
					{
						if (cfg.view_pos + cfg.shift_dir < 0)
						{
							return cfg.col_group_elems_count - 1;
						} else if (cfg.view_pos + cfg.shift_dir > cfg.col_group_elems_count - 1)
						{
							return 0;
						} else
						{
							return cfg.view_pos + cfg.shift_dir;
						}
					} else if (cfg.loop === false)
					{
						if (cfg.view_pos + cfg.shift_dir < 0)
						{
							return -2;
						} else if (cfg.view_pos + cfg.shift_dir > cfg.col_group_elems_count - 1)
						{
							return -2;
						} else
						{
							return cfg.view_pos + cfg.shift_dir;
						}
					}
				} else if (shift_type === 'absolute')
				{
					return shift_dest;
				}
			}();

			if (cfg.view_pos !== -2)
			{
				
				if (cfg.ctrl_pag === true)
				{
					cfg.$ctrl_pag_wrap
						.children()
							.removeClass(egs.add_prefix('current'))
							.eq(cfg.view_pos)
								.addClass(egs.add_prefix('current'));
				}
				
				cfg.shift_busy = 1;
				egs.apply_shift($elem, cfg);
				egs.resume_autoplay($elem, cfg);
			}
			else {
				cfg.view_pos = cfg.prev_view_pos;
			}
		}

		egs.update_slider_ctrl_car_state($elem, cfg);
	}

	egs.set_first_col = function ($elem, cfg)
	{
		//console.log('set rows');
		var a;
		var first_col_class = egs.add_prefix('first-col');

		//reassign first col for every row
		cfg.$col_elems.removeClass(first_col_class);
		
		if (cfg.grid_height === 'auto' || cfg.grid_height === '')
		{
			for (a = 0; a < cfg.col_elem_count; a += 1)
			{
				if (a % cfg.true_cols === 0)
				{
					cfg.$col_elems.eq(a).addClass(first_col_class);
				}
			}
		}
	}

	egs.set_grid_rows = function ($elem, cfg)
	{
		egs.set_first_col($elem, cfg);

		cfg.$col_elems.each(function ()
		{
			var $media = $(this).find('*[class*="' + egs.add_prefix('media-') + '"]');
			var $media_img = $media.find('img');

			if (cfg.grid_height === 'constrain')
			{
				$(this).css({
					height: cfg.col_elem_width * cfg.grid_height_ratio/100,
					overflow: 'hidden'
				});
			} else if (typeof cfg.grid_height === 'number')
			{
				$(this).css({
					height: cfg.grid_height,
					overflow: 'hidden'
				});
			}

			//gridslider supports build in media-x behaviour even though media-x isn't really a part of it anymore
			if ($media.length > 0)
			{
				if ($media_img.length > 0)
				{
					egs.on_image_load_end($media_img, function ()
					{
						//alert('wrap wrap: ' + $media.parent().width() + ' img wrap: ' + $media.get(0).tagName + ' ' + $media.width() + ' ' + $media.attr('width') + ' img: ' + $media_img.get(0).tagName + ' ' + $media_img.width() + ' ' + $media_img.attr('width'));
						egs.init_media ($media_img, cfg.image_stretch_mode, $media, cfg.media_height, cfg.media_height_ratio);
					});
				} else
				{
					//egs.adjust_image_size_and_pos($media_img, $media, cfg.image_stretch_mode);
				}
			}

		});
	}

	egs.init_media = function ($img, img_stretch_mode, $media, media_height, media_height_ratio)
	{
		$img.attr('style', ''); //read on the style attr notes below
		
		if (media_height !== 'auto')
		{
			if (media_height === 'constrain')
			{
				$media.height($media.width() * media_height_ratio / 100);
			} else if (typeof parseInt(media_height) === 'number' && media_height / media_height === 1)
			{
				$media.height(media_height);
			}
		} else
		{
			if ($media.height() > $media.parent().height())
			{
				$media.height($media.parent().height());
			}
		}

		var h = $media.height();
		var w = $media.width();
		var img_w = $img.width();
		var img_h = $img.height();
		var img_ratio;
		var parent_ratio;
		var ratio;

		/*
			NOTE:
				-style attribute may not be the cleanest way here
				-we need to have these attributes passed and native jQuery does not support !important
				-this way style attribute is exclusively taken for purpouses of proper image alignment.
				-since this widget is pretty hermetic it shouldn't be a problem as long as there are no internal conflits
		*/
		switch (img_stretch_mode)
		{
			case 'x':
			{
				if (img_h > h)
				{
					$img.attr('style', 'margin-top: ' + (-(img_h - h) / 2) + 'px !important');
				} else
				{
					$img.attr('style', 'margin-top: ' + ((h - img_h) / 2) + 'px !important');
				}

				break;
			}
			case 'y':
			{
				if (img_w > w)
				{
					$img.attr('style', 'margin-left: ' + (-(img_w - w) / 2) + 'px !important');
				}

				break;
			}
			case 'fill':
			{
				img_ratio = img_w / img_h;
				parent_ratio = w / h;
				ratio = parent_ratio / img_ratio;

				if (ratio >= 1)
				{
					////console.log('>=1')
					$img.attr('style', 'width: ' + w + 'px; ' +  'height: ' + (w / img_ratio) + 'px; ' + 'margin-top: ' + (-(w / img_ratio - h) / 2) + 'px !important;');
				} else
				{
					////console.log('<1')
					$img.attr('style', 'width: ' + (h * img_ratio) + 'px; ' + 'height: ' + h + 'px; ' + 'margin-left: ' + (-(h * img_ratio - w) / 2) + 'px !important;');
				}

				break;
			}
			case 'fit':
			{
				$img.attr('style', 'margin-top: ' + ((h - img_h) / 2) + 'px !important;');
			}
		}
	}

	egs.init_slider_functions = function ($elem, cfg)
	{
		//if (cfg.col_group_elems_count > 1)
		{
			egs.init_autoplay($elem, cfg);

			if ( ! isie || iev > 8)
			{
				$elem
					.swipe({
					     swipeLeft: function()
					     {
					     	egs.init_shift($elem, cfg, 'relative', 1);
					     },
					     swipeRight: function() {
					     	egs.init_shift($elem, cfg, 'relative', -1);
					     }
					});
			}

			$elem
				.bind('mousewheel', function (event, delta, deltaX, deltaY)
				{
					if (cfg.scroll_on_mousewheel === true)
					{
						var shiftdest = -1;

						if (deltaY !== 0 && deltaY < 0 || deltaX !== 0 && deltaX > 0)
						{
							shiftdest = 1;
						}

						egs.init_shift($elem, cfg, 'relative', shiftdest);
						//egs.resume_autoplay($elem, cfg);
						event.preventDefault();
					}
				})

			if (cfg.pause_autoplay_on_hover === true)
			{
				$elem
				.bind('mouseenter', function ()
				{
					egs.pause_autoplay($elem, cfg);
				})
				.bind('mouseleave', function ()
				{
					egs.resume_autoplay($elem, cfg);
				});
			}

			if (cfg.$ctrl_wrap)
			{
				cfg.$ctrl_wrap.find('.' + egs.add_prefix('ctrl'))
					.attr('unselectable', 'on')
					.css({
						'-ms-user-select':'none',
						'-moz-user-select':'none',
						'-webkit-user-select':'none',
						'user-select':'none'
					})
					.bind('click', function()
					{
						this.onselectstart = function()
						{
							return false;
						}

						egs.init_shift($elem, cfg, $(this).data('shifttype'), $(this).data('shiftdest'));
						//egs.resume_autoplay($elem, cfg);

						return false;
					});
			}

			if (cfg.ctrl_external.length > 0)
			{
				cfg.ctrl_external.forEach(function (elem)
				{
					var $elem = elem[0];
					var destination = elem[1];
					var shifttype = (typeof destination === 'number' ? 'absolute' : 'relative');
					
					$elem
						.attr('data-shifttype', shifttype)
						.attr('data-shiftdest', (typeof destination === 'number' ? destination : (destination === 'prev' ? '-1' : '1')))
						.bind('click', function (e)
						{
							if ($(this).data('shiftdest') <= cfg.col_group_elems_count)
							{
								egs.init_shift($elem, cfg, $(this).data('shifttype'), $(this).data('shiftdest'));
								//egs.resume_autoplay($elem, cfg);

								e.preventDefault();
							}
						});
				})
			}
		}
	}

	egs.set_col_groups = function ($elem, cfg)
	{
		if (cfg.slider)
		{
			var a;
			var col_group_class = egs.add_prefix('col-group');

			//egs.on_images_load_end($elem, cfg);

			if (cfg.$col_group_elems && cfg.$col_group_elems.length > 0)
			{
				//console.log(cfg.$col_group_elems)
				//console.log(cfg.$col_group_elems.length);
				//console.log('should not  happen i fslider 0')
				cfg.$col_elems.unwrap();
			}

			for (a = 0; a < cfg.col_elem_count; a += cfg.col_group_elems_capacity)
			{
				$('<div class="' + col_group_class + '"></div>')
					.appendTo(cfg.$col_elems_wrap)
					.append(function ()
					{
						if(a + cfg.col_group_elems_capacity < cfg.col_elem_count)
						{
							return cfg.$col_elems_wrap.children().slice(0, cfg.col_group_elems_capacity);
						} else
						{
							return cfg.$col_elems_wrap.children().slice(0, cfg.col_elem_count - a);
						}
					});
			}

			cfg.$col_group_elems = $elem.find('.' + col_group_class);
			cfg.$col_group_elems
				.eq(cfg.view_pos)
					.css({'z-index': 1, visibility: 'visible'});

			//egs.on_images_load_end($elem, cfg, function ()
			//{
				cfg.col_group_elems_height = [];
				cfg.$col_group_elems.each(function (id)
				{
					cfg.col_group_elems_height.push(cfg.$col_group_elems.eq(id).outerHeight() - cfg.col_spacing_size * cfg.col_spacing_enable);
				});
			//});
		//egs.log('set_col_groups');
		}
	};

	egs.update_slider_ctrl = function ($elem, cfg)
	{
		var car_width;
		var car_height;
		var pag_width;
		var pag_height;

		var pag_pos_x;
		var pag_pos_y;
		var car_pos_x;
		var car_pos_y;

		var anim_speed = cfg.slider_ctrl_first_update !== true ? 0 : cfg.scroll_speed;

		cfg.slider_ctrl_updated = true;

		if (cfg.slider_ctrl_first_init !== true)
		{
			//console.log('booger spawn');
			return false;
		}

		if (cfg.col_group_elems_height !== undefined && cfg.view_pos !== undefined)
		{
			cfg.elem_height = cfg.col_group_elems_height[cfg.view_pos];
		}

		if (cfg.slider && cfg.ctrl_active)
		{
			if (cfg.col_group_elems_count > 1)
			{
				egs.show_slider_ctrl($elem, cfg);
			} else
			{
				egs.hide_slider_ctrl($elem, cfg, 0);
			}

			if (cfg.ctrl_pag === true)
			{
				cfg.$ctrl_pag_wrap
				.children()
					.eq(cfg.view_pos).addClass(egs.add_prefix('current'))
					.end()
					.css({display: 'block'})
					.slice(cfg.col_group_elems_count)
						.css({display: 'none'})
					.end()
				.end()
				.css({
					width: function ()
					{
						return cfg.col_group_elems_count * cfg.ctrl_pag_elem_width;
					}
				});

				pag_width = cfg.$ctrl_pag_wrap.outerWidth();
				pag_height = cfg.ctrl_pag_elem_width + cfg.ctrl_pag_spacing;

				pag_pos_x = (cfg.ctrl_pag_pos_x === 'left' ? 0 + cfg.ctrl_padding : (cfg.ctrl_pag_pos_x === 'right' ? cfg.elem_width - pag_width - cfg.ctrl_padding : cfg.elem_width / 2 - pag_width / 2));
				pag_pos_y = (cfg.ctrl_pag_pos_y === 'top' ? 0 + cfg.ctrl_padding : (cfg.ctrl_pag_pos_y === 'bottom' ? cfg.elem_height - pag_height - cfg.ctrl_padding : cfg.elem_height / 2 - pag_height / 2));

				pag_pos_x += cfg.ctrl_pag_pos_shift_x;
				pag_pos_y += cfg.ctrl_pag_pos_shift_y;
			}

			if (cfg.ctrl_arrows === true)
			{
				car_width = cfg.$ctrl_car_wrap.outerWidth();
				car_height = cfg.ctrl_car_elem_width + cfg.ctrl_arrows_spacing;

				car_pos_x = (cfg.ctrl_arrows_pos_x === 'left' ? 0 + cfg.ctrl_padding : (cfg.ctrl_arrows_pos_x === 'right' ? cfg.elem_width - car_width - cfg.ctrl_padding : cfg.elem_width / 2 - car_width / 2));
				car_pos_y = (cfg.ctrl_arrows_pos_y === 'top' ? 0 + cfg.ctrl_padding : (cfg.ctrl_arrows_pos_y === 'bottom' ? cfg.elem_height - car_height - cfg.ctrl_padding : cfg.elem_height / 2 - car_height / 2));

				car_pos_x += cfg.ctrl_arrows_pos_shift_x;
				car_pos_y += cfg.ctrl_arrows_pos_shift_y;
			}

			if (cfg.ctrl_arrows === true && cfg.ctrl_pag === true && cfg.ctrl_arrows_full_width === false)
			{
				if (cfg.ctrl_arrows_pos_x === cfg.ctrl_pag_pos_x && cfg.ctrl_arrows_pos_y === cfg.ctrl_pag_pos_y)
				{
					if (cfg.ctrl_arrows_pos_y === 'bottom')
					{
						car_pos_y -= pag_height;
					} else
					{
						pag_pos_y += car_height;
					}
				}
			}

			if (cfg.ctrl_arrows === true)
			{
				if (cfg.ctrl_arrows_full_width === true)
				{
					cfg.$ctrl_car_wrap.stop(true, true).animate({
						width: cfg.elem_width - 2 * cfg.ctrl_padding,
						top: car_pos_y,
						left: cfg.ctrl_padding
					}, anim_speed);
				} else
				{
					cfg.$ctrl_car_wrap.stop(true, true).animate({
						top: car_pos_y,
						left: car_pos_x
					}, anim_speed);
				}
			}

			if (cfg.ctrl_pag === true)
			{
				cfg.$ctrl_pag_wrap.stop(true, true).animate({
					top: pag_pos_y,
					left: pag_pos_x
				}, anim_speed);
			}

			//console.log('elem height: ' + cfg.elem_height + ' pag_pos_y: ' + pag_pos_y + ' car_pos_y: ' + car_pos_y)

			egs.update_slider_ctrl_car_state($elem, cfg);

			if (cfg.slider_ctrl_first_update !== true)
			{
				//console.log('show once');

				egs.show_slider_ctrl($elem, cfg);

				cfg.slider_ctrl_first_update = true;
			}
		}

		//egs.log('update_slider_ctrl');
	}

	egs.update_slider_ctrl_car_state = function ($elem, cfg)
	{
		var $ctrl_prev;
		var $ctrl_next;

		if (cfg.ctrl_arrows === true && cfg.loop === false)
		{
			$ctrl_prev = cfg.$ctrl_car_wrap.children('.' + egs.add_prefix('prev'));
			$ctrl_next = cfg.$ctrl_car_wrap.children('.' + egs.add_prefix('next'));

			if (cfg.view_pos === cfg.col_group_elems_count - 1)	
			{
				$ctrl_next.addClass(egs.add_prefix('disabled'));
			} else 
			{
				$ctrl_next.removeClass(egs.add_prefix('disabled'));
			}

			if (cfg.view_pos === 0)	
			{
				$ctrl_prev.addClass(egs.add_prefix('disabled'));
			} else 
			{
				$ctrl_prev.removeClass(egs.add_prefix('disabled'));
			}
		}
	}

	egs.show_slider_ctrl = function ($elem, cfg, speed)
	{
		speed === undefined ? speed = cfg.scroll_speed : '';
		
		if (cfg.ctrl_active && cfg.col_group_elems_count > 1)
		{
			if (cfg.ctrl_hide_sentinel)
			{
				clearTimeout(cfg.ctrl_hide_sentinel);
			}

			cfg.$ctrl_wrap.stop(true,true).fadeIn({
				duration: speed,
				complete: function () 
				{
					cfg.ctrl_visible = true;
				}
			});
		}
	}

	egs.hide_slider_ctrl = function ($elem, cfg, speed)
	{
		speed === undefined ? speed = cfg.scroll_speed : '';

		if (cfg.ctrl_active)
		{
			if (cfg.ctrl_hide_sentinel)
			{
				clearTimeout(cfg.ctrl_hide_sentinel);
			}

			cfg.ctrl_hide_sentinel = setTimeout(function ()
			{
				cfg.$ctrl_wrap.stop(true,true).fadeOut({
					duration: speed,
					complete: function () 
					{
						cfg.ctrl_visible = false;
					}
				});
			}, (speed === 0 ? 0 : cfg.ctrl_hide_delay));
		}
	}

	egs.init_slider_ctrl = function ($elem, cfg)
	{
		cfg.slider_ctrl_first_init = false;
		cfg.ctrl_car_elem_width = 0;
		cfg.ctrl_pag_elem_width = 0;
		cfg.$ctrl_wrap = null;
		cfg.$ctrl_pag_wrap = null;
		cfg.$ctrl_car_wrap = null;
		cfg.$ctrl_visible = false;

		if (cfg.ctrl_arrows === true || cfg.ctrl_pag === true)
		{
			cfg.ctrl_active = true;
		} else
		{
			return false;
		}

		var ctrl_class = egs.add_prefix('ctrl');
		var ctrl_wrap_class = egs.add_prefix('ctrl-wrap');
		var ctrl_pag_class = egs.add_prefix('ctrl-pag');
		var ctrl_car_class = egs.add_prefix('ctrl-car');
		var ctrl_next_class = egs.add_prefix('next');
		var ctrl_prev_class = egs.add_prefix('prev');

		$('<div class="' + ctrl_wrap_class + '"></div>')
			.appendTo($elem)
			.addClass(egs.add_prefix('ctrl-style-' + cfg.ctrl_style + (cfg.theme === 'light' ? '' : '-light')));

		cfg.$ctrl_wrap = $elem.find('.' + ctrl_wrap_class);

		egs.hide_slider_ctrl($elem, cfg, 0);

		cfg.$elem.addClass(egs.add_prefix('ctrl-car-pos-' + cfg.ctrl_pos_y + '-' + cfg.ctrl_pos_x));
		cfg.$elem.addClass(egs.add_prefix('ctrl-pag-pos-' + cfg.ctrl_pos_y + '-' + cfg.ctrl_pos_x));

		if (cfg.ctrl_arrows === true) {
			cfg.$ctrl_wrap
				.append('<div class="' + ctrl_car_class + '"><div class="' + ctrl_class + ' ' + ctrl_prev_class + '" data-shifttype="relative" data-shiftdest="-1"></div><div class="' + ctrl_next_class + ' ' + ctrl_class + '" data-shifttype="relative" data-shiftdest="1"></div></div>');
			
			cfg.$ctrl_car_wrap = cfg.$ctrl_wrap.find('.' + ctrl_car_class);

			cfg.$ctrl_car_wrap.css({
				top: 0,
				left: 0
			});

			cfg.$ctrl_car_wrap.children().each(function ()
			{
				$(this).css({margin: cfg.ctrl_arrows_spacing / 2});
			});

			cfg.ctrl_car_elem_width = cfg.$ctrl_car_wrap.children().outerWidth() + cfg.ctrl_arrows_spacing;

			if (cfg.ctrl_arrows_full_width === true)
			{
				//cfg.$ctrl_car_wrap.addClass(egs.add_prefix('full-width'));
				//cfg.$ctrl_car_wrap.width(cfg.elem_width);
			} else
			{
				cfg.$ctrl_car_wrap.width(cfg.ctrl_car_elem_width * 2);
			}
		}

		if (cfg.ctrl_pag === true)
		{
			cfg.$ctrl_wrap
				.append(function ()
				{
					var a;
					var result = '';

					result += '<div class="' + ctrl_pag_class + '">';
					for (a = 0; a < cfg.col_elem_count; a += 1)
					{
						result += '<div class="' + ctrl_class + '" data-shifttype="absolute" data-shiftdest="' + a + '"></div>';
					}
					result += '</div>';

					return result;
				});

			cfg.$ctrl_pag_wrap = cfg.$ctrl_wrap.find('.' + ctrl_pag_class);

			cfg.$ctrl_pag_wrap.css({
				top: 0,
				left: 0
			});

			cfg.$ctrl_pag_wrap.children().each(function ()
			{
				$(this).css({margin: cfg.ctrl_pag_spacing / 2});
			});

			cfg.ctrl_pag_elem_width = cfg.$ctrl_pag_wrap.children().outerWidth() + cfg.ctrl_pag_spacing;
		}

		cfg.$ctrl_wrap.find('.' + egs.add_prefix('ctrl'))
			.attr('unselectable', 'on')
			.css({
				'-ms-user-select':'none',
				'-moz-user-select':'none',
				'-webkit-user-select':'none',
				'user-select':'none'
			});

		if (cfg.ctrl_always_visible === false)
		{
			$elem
				.bind('mouseenter', function ()
				{
					egs.show_slider_ctrl($elem, cfg);
				})
				.bind('mouseleave', function ()
				{
					egs.hide_slider_ctrl($elem, cfg);
				});
		}

		cfg.slider_ctrl_first_init = true;

		if (cfg.slider_ctrl_updated === true)
		{
			//console.log('booger patch');
			egs.update_slider_ctrl($elem, cfg);
		}
	}

	egs.init_load_overlay = function ($elem, cfg)
	{
	}

	egs.init_slider_structure = function ($elem, cfg)
	{
		var slider_class;
		var slider_window_class;
		var load_overlay_class;

		cfg.elem_width = $elem.outerWidth();

		if (cfg.slider === true)
		{
			//console.log('init_slider');
			slider_class = egs.add_prefix('slider');
			slider_window_class = egs.add_prefix('slider-window');
			load_overlay_class = egs.add_prefix('load-overlay');

			$elem
				.addClass(slider_class)
					.children()
						.wrapAll('<div class="' + slider_window_class + '"></div>');

			cfg.$slider_window = $elem.find('.' + slider_window_class);
			cfg.$slider_window
				.css({height: 20, overflow: 'hidden'})
				.append(function ()
				{
					if ($(this).find('.' + load_overlay_class).length === 0)
					{
						return '<div class="' + load_overlay_class + '"></div>';
					}
				});

			cfg.$slider_window.find(load_overlay_class).show();

			egs.on_images_load_end($elem, cfg, function ()
			{
				egs.set_col_groups($elem, cfg);

				cfg.$slider_window
					.css({overflow: 'hidden' })
					.children('.' + load_overlay_class).delay(cfg.scroll_speed).fadeOut(1000).end()
					.queue(function ()
					{
						$(this)
							.find('.' + load_overlay_class).remove()
							.css({'overflow': 'visible'})
							.dequeue();
					});

				egs.set_slider_window_height($elem, cfg);
				egs.update_slider_ctrl($elem, cfg);
			});

			egs.init_slider_ctrl($elem, cfg);
			egs.init_slider_functions($elem, cfg);
		}
	}

	egs.get_grid_data = function ($elem, cfg)
	{
		cfg.elem_width = $elem.width();

		cfg.$col_elems_wrap = $elem.find('.' + egs.add_prefix('cols')).eq(0);

		cfg.$col_elems = function ()
		{
			if( ! cfg.$col_elems_wrap.children('.' + egs.add_prefix('col')).length)
			{
				return cfg.$col_elems_wrap.children().children('.' + egs.add_prefix('col'));
			} else
			{
				return cfg.$col_elems_wrap.children()
			}
		}();
		cfg.col_elem_count = cfg.$col_elems.length;
		cfg.col_elem_width = cfg.$col_elems.outerWidth();
		cfg.img_count = $elem.find('img').length;
		cfg.true_cols = Math.round(cfg.elem_width / cfg.col_elem_width);

		if ($(window).width() < 580)
		{
			if(!cfg.original_rows)
			{
				cfg.original_rows = cfg.rows;
			}
			cfg.rows = 1;
		}

		if ($(window).width() > 580 && typeof cfg.original_rows === 'number' && cfg.rows !== cfg.original_rows)
		{
			cfg.rows = cfg.original_rows;
		}

		cfg.col_group_elems_capacity = cfg.rows * cfg.true_cols;
		cfg.col_group_elems_count = Math.ceil(cfg.col_elem_count/cfg.col_group_elems_capacity);

		if (cfg.view_pos >= cfg.col_group_elems_count)
		{
			cfg.view_pos = 0;
		}

		//egs.log('get_grid_data');

		//egs.log('col_elems_wrap: ' + cfg.$col_elems_wrap.length + ' col_elems: ' + cfg.$col_elems.length + ' col_elem_count: ' + cfg.col_elem_count + ' col_elem_width: ' + cfg.col_elem_width + ' true_cols : ' + cfg.true_cols);
	}

	egs.generate_rules = function (obj)
	{
		var result = '';

		for(var selector in obj)
		{
			result += selector + ' { \n';
			for(var prop in obj[selector])
			{
				result += '	' + prop + ': ' + obj[selector][prop] + '; \n';
			}
			result += '} \n';
		}

		return result;
	}

	egs.generate_stylesheet_content = function ($elem, cfg)
	{
		var result = '';
		var styles = {};
		var ie7_styles = {};
		var grid_class = egs.add_prefix('grid');
		var cols_class = egs.add_prefix('cols');
		var col_class = egs.add_prefix('col');
		var ie7_grid_fix_class = egs.add_prefix('col');

		var make_selector = function ()
		{
			return 'asdf';
		}

		if (cfg.col_spacing_size !== 30 && cfg.col_spacing_enable !== 0)
		{
			styles['.' + grid_class + cfg.elem_selector + ' .' + cols_class] =
			{
				margin: -(cfg.col_spacing_size/2) + 'px'
			};
			styles['.' + grid_class + cfg.elem_selector  + ' .' +  cols_class + ' .' + col_class] =
			{
				padding: (cfg.col_spacing_size/2) + 'px'
			};

			result += egs.generate_rules(styles);

			if (isie && parseInt(iev) == 7)
			{
				ie7_styles['.' + ie7_grid_fix_class + '.cols-wrap'] =
				{
					margin: -(cfg.col_spacing_size/2) + 'px !important'
				},
				ie7_styles['.' + ie7_grid_fix_class + '.cols-wrap .' + col_class + ' > *:first-child'] =
				{
					padding: (cfg.col_spacing_size/2) + 'px !important'
				}

				result += egs.generate_rules(ie7_styles);
			}
		}
		if (cfg.width !== 'auto')
		{
			if (typeof cfg.width === 'number')
			{
				cfg.width += 'px';
			}
			result += cfg.elem_selector + ' { width: ' + cfg.width + '; }';
		}

		return result;
	}

	egs.init_gallery_title = function ($elem, cfg)
	{
		var $img = $elem.find('img');
		var img_class = egs.add_prefix('img-title');

		$img.each(function ()
		{
			if ($(this).siblings('.' + img_class).length === 0)
			{
				var title = $(this).attr('title');
				var alt = $(this).attr('alt');
				var result = '';
				var img_wrap_h = $(this).parents('.' + egs.add_prefix('col')).eq(0).outerHeight();

				if (title !== undefined)
				{
					result = title;
				} else if (alt !== undefined)
				{
					result = alt;
				}

				if (result !== '')
				{
					$('<span class="' + img_class + '">' + result + '</span>')
						.appendTo($(this).parent())

					var $title = $(this).siblings('.' + img_class);
					var title_h = $title.outerHeight();
					
					var pos_y_hidden = (cfg.img_title_pos_y === 'bottom' ? img_wrap_h : -title_h);
					var pos_y_visible = (cfg.img_title_pos_y === 'bottom' ? img_wrap_h - title_h : 0)

					if (cfg.always_show_img_title === false)
					{
						$title
							.css({
								opacity: 0,
								top: pos_y_hidden
							});

						$(this).parent().on('mouseenter', function ()
						{
							$title
								.stop(true, true).animate({
									opacity: 1,
									top: pos_y_visible
								}, 500);
						})
						.bind('mouseleave', function ()
						{
							$title .delay(250).animate({
								opacity: 0,
								top: pos_y_hidden
							}, 500);
						});
					}
				}
			}
		});
	}

	egs.slider_is_ready = function ($elem, cfg)
	{
		return cfg.all_images_loaded;
	}

	egs.on_images_load_end = function ($elem, cfg, callback)
	{
		if (cfg.img_count > 0)
		{
			if (cfg.all_images_loaded !== true)
			{
				var loaded = 0;
				var locked = 0;
				var broken = 0;
				var $img = $elem.find('img');
				var img_count = $img.length;

				$img.each(function (id)
				{
					$(this)
						.bind('load', function ()
						{
							loaded += 1;
							//what was the (loaded !== img_count && id === img_count - 1 && locked === 0) thing for?
							//if ((loaded === img_count && locked === 0) || (loaded !== img_count && id === img_count - 1 && locked === 0))
							if ((loaded === img_count && locked === 0))
							{
								locked = 1;
								cfg.all_images_loaded = true;

								callback ? callback($elem, cfg) : '';

								//console.log('all ' + loaded + ' of ' + cfg.img_count + ' detected images have loaded.' + (broken ? '(' + broken + ') links seem to be broken ;(' : ''));
							}
						})
						.bind('error', function ()
						{
							broken += 1;
							$(this).unbind('error').attr('src', egs.placeholder_img_data);
						});

					if ((typeof this.complete != 'undefined' && this.complete) || (typeof this.naturalWidth != 'undefined' && this.naturalWidth > 0))
					{
						$(this)
							.trigger('load')
							.unbind('load');
					}
				});
			}  else
			{
				callback ? callback($elem, cfg) : '';
			}
		} else
		{
			callback ? callback($elem, cfg) : '';
		}
	}

	egs.on_image_load_end = function ($img, callback)
	{
		$img.each(function ()
		{
			$(this).bind('load', function ()
			{
				callback();
			}).bind('error', function ()
			{
				 $(this)
				 	.unbind('error')
				 	.attr('src', egs.placeholder_img_data);
			});

			if ((typeof this.complete != 'undefined' && this.complete) || (typeof this.naturalWidth != 'undefined' && this.naturalWidth > 0))
			{
				$(this)
					.trigger('load')
					.unbind('load');
			}
		});
	}

	egs.update_elem = function ($elem, cfg)
	{
		egs.get_grid_data($elem, cfg);
		egs.set_grid_rows($elem, cfg);

		if (cfg.gallery_img_title === true)
		{
			egs.init_gallery_title($elem, cfg);
		}

		if (cfg.slider === true)
		{
			egs.set_col_groups($elem, cfg);
			egs.set_slider_window_height($elem, cfg);
			egs.update_slider_ctrl($elem, cfg);
		}
		//egs.log('update');
	}

	egs.add_elements = function ($gridslider_elem, $target_elem, remove_target, callback)
	{
		var re = new RegExp(egs.prefix + '-id-(\\d+)');

		$gridslider_elem.each(function ()
		{
			var cfg = egs.elem_cfg[$(this).attr('class').match(re)[1]];

			if (cfg.slider === true && cfg.$col_group_elems.length === 0)
			{
				$('<div class="' + egs.add_prefix('col-group') + '"></div>')
					.appendTo(cfg.$col_elems_wrap)

				cfg.$col_group_elems = cfg.$col_elems_wrap.children();
			}

			$target_elem.each(function ()
			{
				var $col_elem = $('<div class="' + egs.add_prefix('col') +'"></div>').append($(this).clone(true, true));

				if (cfg.slider === true)
				{
					if (cfg.$col_group_elems.eq(-1).children().length < cfg.col_group_elems_capacity)
					{
						$col_elem
							.appendTo(cfg.$col_group_elems.eq(-1))
							.hide()
							.fadeIn(500);
					} else
					{
						$('<div class="' + egs.add_prefix('col-group') + '"></div>')
							.appendTo(cfg.$col_elems_wrap)
						$col_elem
							.appendTo(cfg.$col_elems_wrap.children().eq(-1))
							.hide()
							.fadeIn(500)
					}
				} else
				{
					$col_elem
						.appendTo(cfg.$col_elems_wrap)
						.hide()
						.fadeIn(500);
				}
			});

			egs.update_elem(cfg.$elem, cfg);

			if (callback)
			{
				callback();
			}
		});

		if (remove_target === true)
		{
			$target_elem.remove();
		}
	}

	egs.remove_elements = function ($gridslider_elem, targets_id, callback)
	{
		var re = new RegExp(egs.prefix + '-id-(\\d+)');

		$gridslider_elem.each(function ()
		{
			var cfg = egs.elem_cfg[$(this).attr('class').match(re)[1]];

			if (typeof targets_id === 'number')
			{
				if (cfg.$col_elems.eq(targets_id).length > 0)
				{
					cfg.$col_elems.eq(targets_id)
						//.fadeOut(500)
						//.queue(function ()
						//{
						//	$(this)
								.remove()
						//		.dequeue();
						//});
				}
			} else
			{
				targets_id.forEach(function (elem)
				{
					if (cfg.$col_elems.eq(targets_id[elem]).length > 0)
					{
						cfg.$col_elems.eq(targets_id[elem])
							//.fadeOut(500)
							//.queue(function ()
							//{
							//	$(this)
									.remove()
							//		.dequeue();
							//});
					}
				});
			}

			if (cfg.$col_group_elems.eq(-1).children().length === 0)
			{
				cfg.$col_group_elems.eq(-1).remove();
			}

			egs.update_elem($(this), cfg);

			if (callback)
			{
				callback();
			}
		});
	}

	egs.pause_autoplay = function ($elem, cfg)
	{
		cfg.autoplay_active = 0;
		clearTimeout(cfg.autoplay_stamp);
	}

	egs.resume_autoplay = function ($elem, cfg)
	{
		clearTimeout(cfg.autoplay_stamp);
		if (cfg.autoplay_enable === true && cfg.force_autoplay_pause !== 1)
		{
			cfg.autoplay_active = 1;
			cfg.autoplay_stamp = setTimeout(function ()
			{
				egs.init_shift($elem, cfg, 'relative', cfg.autoplay_shift_dir);
			}, cfg.autoplay_interval * 1000);
		}
	}

	egs.init_autoplay = function ($elem, cfg)
	{
		if (cfg.autoplay_enable === true)
		{
			$(window)
				.bind('load', function ()
				{
					if (egs.in_view($elem))
					{
						egs.resume_autoplay($elem, cfg);
					}
				})
				.bind('blur', function ()
				{
					egs.pause_autoplay($elem, cfg);
				})
				.bind('focus', function ()
				{
					egs.resume_autoplay($elem, cfg);
				})
				.bind('scroll', function ()
				{
					if (!egs.in_view($elem))
					{
						if (cfg.autoplay_active === 1 && cfg.autoplay_enable === true)
						{
							egs.pause_autoplay($elem, cfg)
						}
					} else
					{
						if (cfg.autoplay_active !== 1  && cfg.autoplay_enable === true)
						{
							egs.resume_autoplay($elem, cfg)
						}
					}
				});
		} else
		{
			clearTimeout(cfg.autoplay_stamp)
			cfg.autoplay_active = 0;
		}
	}

	egs.class_string_from_arr = function (arr)
	{
		var result = '';

		arr.forEach(function (elem)
		{
			result += egs.add_prefix(elem) + ' ';
		});

		return result;
	}

	egs.init_grid_structure = function ($elem, cfg)
	{
		var target_elem_classes = function ()
		{
			var arr = [
				egs.add_prefix('grid'),
				egs.add_prefix('scroll-axis-' + cfg.scroll_axis),
				egs.add_prefix('grid-height-' + cfg.grid_height),
				egs.add_prefix('image-stretch-mode-' + cfg.image_stretch_mode),
				egs.add_prefix('align' + cfg.align)
			];

			return arr.join(' ');
		};

		var cols_container_classes = function ()
		{
			var arr = [
				egs.add_prefix('cols'),
				egs.add_prefix('cols-' + cfg.cols),
				egs.add_prefix('rows-' + cfg.rows),
				egs.add_prefix('spacing-' + (cfg.col_spacing_enable === true ? 1 : 0))
			];

			return arr.join(' ');
		}

		if (egs.browser_msie_7)
		{
			$elem.addClass(egs.add_prefix('ie7-grid-fix'));
		}

	 	if ($elem.find('.' + egs.add_prefix('cols')).length === 0)
	 	{
	 		if (cfg.hide_grid_cell_overflow === true)
	 		{
		 		$elem.children().each(function ()
		 		{
		 			if ($(this).prop('tagName') === 'IMG')
		 			{
		 				$(this).wrap('<span class="' + egs.add_prefix('hide-grid-cell-overflow') + '"></span>');

		 			} else if ($(this).children().length === 1 && $(this).children().prop('tagName') === 'IMG' && $(this).css('overflow') !== 'hidden')
 					{
 						$(this).css({overflow: 'hidden'});
 					}
		 		});
		 	}

	 		$elem
	 			.addClass(target_elem_classes())
		 		.children()
		 			.wrapAll('<div class="' + cols_container_classes() + '"></div>')
			 		.wrap('<div class="' + egs.add_prefix('col') + '"></div>');
		}

		if (!egs.css)
		{
			egs.css = '';
		}
		egs.css += egs.generate_stylesheet_content($elem, cfg);

		$(egs.style_destination)
			.find('.' + egs.add_prefix('gridslider-custom-styles')).remove().end()
			.append('<style class="' + egs.add_prefix('gridslider-custom-styles') + '">' + egs.css + '</style>');
		
		egs.get_grid_data($elem, cfg);
		egs.set_grid_rows($elem, cfg);

		if (cfg.gallery_img_title === true)
		{
			egs.on_images_load_end($elem, cfg, function () 
			{
				egs.init_gallery_title($elem, cfg);
			});
		}
	}

	egs.assign_gridslider_id = function ()
	{
		var id = Math.round(Math.random()*10000);

		if ( ! egs.elem_cfg[id])
		{
			return id;
		} else
		{
			egs.assign_gridslider_id();
			return false;
		}
	}

	egs.init_gridslider = function ($elem, cfg)
	{
		if ( ! $elem.attr('data-egs'))
		{
			//console.log('start egs initialization');

			$elem.attr('data-egs', true);

			if ( ! egs.style_destination)
			{
				if (isie && iev < 9) {
					egs.style_destination = 'body';
				}
				else {
					egs.style_destination = 'head';
				}
			}

			if (egs.browser_msie_7 === undefined)
			{
				egs.browser_msie_7 = (isie && parseInt(iev) == 7);
			}

			if ( ! cfg.elem_id)
			{
				cfg.elem_id = egs.assign_gridslider_id();
				$elem.addClass(egs.add_prefix('id-' + cfg.elem_id));
			}
			cfg.$elem = $elem;
			if ( ! cfg.defaults)
			{
				cfg.defaults = jQuery.extend({}, true, cfg);
			}
			egs.elem_cfg[cfg.elem_id] = cfg;
			egs.init_grid_structure($elem, cfg);
			egs.init_slider_structure($elem, cfg);

			$(window).bind('resize.egs', function ()
			{
				egs.update_elem($elem, cfg);
			});
			//console.log('end egs initialization');
		} else
		{
			//console.log('there\'s a gridslider for this elem defined already!');
		}
	}

	egs.uninit_gridslider = function (id)
	{
		var cfg = egs.elem_cfg[id];
		var css_dep = [
			'scroll-axis-',
			'grid-height-',
			'image-stretch-mode-',
			'align'
		];

		if (cfg.slider == 0)
		{
			cfg.$col_elems.children().unwrap().unwrap();
		} else if (cfg.slider == 1)
		{
			cfg.$col_elems.children().unwrap().unwrap().unwrap().unwrap();
			if (cfg.$ctrl_wrap && cfg.$ctrl_wrap.length > 0)
			{
				cfg.$ctrl_wrap.remove();
			}
		}

		if (cfg.gallery_img_title == 1)
		{
			cfg.$elem.find('.' + egs.add_prefix('img-title')).remove();
		}

		css_dep.forEach(function (name)
	 	{
	 		var class_prop = cfg.$elem.prop('class');
	 		var re = new RegExp(egs.add_prefix(name + '\\S+\\s*'))
	 		cfg.$elem.prop('class', class_prop.replace(re, ''))
	 	});

	 	cfg.$elem.attr('data-egs', '');

	 	$(window).unbind('resize.egs' + id);
	}

	egs.reinit_gridslider = function (id)
	{
		var $elem = egs.elem_cfg[id].$elem;
		var new_cfg = jQuery.extend({}, true, egs.elem_cfg[id].defaults);

		new_cfg.elem_id = id;

		for (var key in cfg)
		{
			new_cfg[key] = cfg[key];
		}

		egs.uninit_gridslider(id);
	}

    $.fn.gridSlider = function(options) {

        var defaults = {
        	elem_selector: $(this).selector,
            slider: true,
			cols: 1,
			rows: 1,
			width: 'auto',
			align: 'auto',
			col_spacing_enable: true,
			col_spacing_size: 30,
			ctrl_arrows: true,
			ctrl_pag: true,
			ctrl_external: [],
			ctrl_always_visible: false,
			ctrl_arrows_pos_x: 'center',
			ctrl_arrows_pos_y: 'top',
			ctrl_arrows_spacing: 2,
			ctrl_arrows_pos_shift_x: 0,
			ctrl_arrows_pos_shift_y: 0,
			ctrl_arrows_full_width: false,
			ctrl_pag_pos_x: 'center',
			ctrl_pag_pos_y: 'top',
			ctrl_pag_spacing: 2,
			ctrl_pag_pos_shift_x: 0,
			ctrl_pag_pos_shift_y: 0,
			ctrl_padding: 8,
			ctrl_style: 0,
			ctrl_hide_delay: 1000,
			scroll_axis: 'x',
			transition: 'slide',
			easing: 'swing',
			scroll_speed: 500,
			autoplay_enable: false,
			autoplay_interval: 5,
			autoplay_shift_dir: 1,
			pause_autoplay_on_hover: true,
			view_pos: 0,
			grid_height: 'auto',
			grid_height_ratio: 100,
			media_height: 'auto',
			media_height_ratio: 100,
			image_stretch_mode: 'auto',
			gallery_img_title: false,
			always_show_img_title: false,
			loop: true,
			hide_grid_cell_overflow: false,
			scroll_on_mousewheel: true,
			theme: 'light',
			img_title_pos_x: 'left',
			img_title_pos_y: 'bottom'
			//ctrl_autoplay_toggle: 0,
			//ctrl_timer: 0,
			//ctrl_mode: 'compact',
			//trigger_delay: 0,
			//timeout_delay: 0,
        }

		var options = $.extend(defaults, options);

		return this.each(function()
		{
			egs.init_gridslider($(this), jQuery.extend(true, {}, options));
		});
    }
})(jQuery);

//gridslider_end