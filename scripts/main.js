$(document).ready(function(){
	$(".button-collapse").sideNav();
    

        //GridSlider Init
        $(".comp_logos").gridSlider({
                // align: 'center',
                grid_height: 180,
                // hide_grid_cell_overflow: true,
                cols: 4,
                rows: 2,
                transition: 'slide', //)
                scroll_axis: 'x', //Scroll axis
                scroll_speed: 1000, //Scroll speed in miliseconds
                loop: false, //Behaviour of the slider on the last-to-first and first-to-last element transition
                ctrl_arrows_pos_y: 'center', //Vertical position of slider arrows
                ctrl_arrows_pos_x: 'center', //Horizontal position of slider arrows
                ctrl_always_visible: false, //Slider controls visible at all times or on hover only
                ctrl_style: 2, //Style of navigation (for custom one provide path to filename)
                ctrl_pag: false,
                ctrl_arrows_full_width: true,
                theme: 'light',
                scroll_on_mousewheel: false,
        });


        $('select').material_select();
        $(".dropdown-button").dropdown({
            hover: false,
        });

        $('ul.tabs').tabs();
          //   jQuery('ul.sf-menu').superfish({
          //   animation: {height:'show'}, // slide-down effect without fade-in
          //   delay:     1200     // 1.2 second delay on mouseout
          // });

         $("#fakeloader").fakeLoader({
            timeToHide:1200, //Time in milliseconds for fakeLoader disappear
            zIndex:"9999999",//Default zIndex
            spinner:"spinner1",//Options: 'spinner1', 'spinner2', 'spinner3', 'spinner4', 'spinner5', 'spinner6', 'spinner7'
            bgColor:"#2ecc71", //Hex, RGB or RGBA colors
          });


 // The slider being synced must be initialized first
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 190,
    itemMargin: 50,
    asNavFor: '#slider',
  });
 
  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
		// itemHeight: 300,
		itemWidth: 600,
    sync: "#carousel"
  });

$(".fancybox-button").fancybox();


    // Range slider
    $("#range_24").ionRangeSlider({
        type: "double",
        min: 1000,
        max: 2000,
        from: 1,
        to: 2000,
        hide_min_max: true,
        hide_from_to: false,
        grid: false
    });
});

$(document).on('click', '#click_event', function(){
    $('body').css({
        'overflow-y': 'scroll',
    });
});

